module bitbucket.org/canonical-ledgers/fct-address-monitord

require (
	github.com/AdamSLevy/factom v0.0.0-20180830194820-f09dea5bd165
	github.com/canonical-ledgers/bitcointax v1.0.2
	github.com/canonical-ledgers/cryptoprice v1.0.2
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/gocraft/dbr v0.0.0-20180507214907-a0fd650918f6
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/posener/complete v1.1.2
	github.com/sirupsen/logrus v1.0.6
	golang.org/x/crypto v0.0.0-20180830192347-182538f80094 // indirect
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d // indirect
	golang.org/x/sys v0.0.0-20180830151530-49385e6e1522 // indirect
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
)
