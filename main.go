package main

import (
	"os"
	"os/signal"

	"bitbucket.org/canonical-ledgers/fct-address-monitord/db"
	"bitbucket.org/canonical-ledgers/fct-address-monitord/flag"
	"github.com/sirupsen/logrus"
)

func main() { os.Exit(_main()) }

func _main() int {
	flag.Parse()
	// Attempt to run the completion program.
	if flag.Completion.Complete() {
		// The completion program ran, so just return.
		return 0
	}
	flag.Validate()

	// Set up local logger
	_log := logrus.New()
	_log.Formatter = &logrus.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if flag.LogDebug {
		_log.SetLevel(logrus.DebugLevel)
	}
	log := _log.WithField("pkg", "main")

	// Start the db engine.
	dbErrorExit, err := db.Start()
	if err != nil {
		log.Fatalf("db.Start(): %v", err)
	}

	log.Info("Factoid Address Monitord started.")
	defer log.Info("Factoid Address Monitord stopped.")

	// Set up interrupts channel.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	ret := 0
	select {
	case <-sig:
		log.Infof("SIGINT: Shutting down now.")
		db.Stop()
	case <-dbErrorExit:
	}

	return ret
	return 0
}
