-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE metadata
(
  id            INT  DEFAULT 0 PRIMARY KEY,
  fblock_height INT  DEFAULT 0,
  total_income  REAL DEFAULT 0,
  total_fct     REAL DEFAULT 0
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE metadata;
