-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE transactions
(
        date         DATETIME NOT NULL,
	action       TEXT NOT NULL,
	symbol       TEXT NOT NULL,
	currency     TEXT NOT NULL,
	volume       REAL NOT NULL,
	exchange     TEXT,
	exchange_id  TEXT,
	price        REAL NOT NULL,
	total        REAL NOT NULL,
	fee          REAL DEFAULT 0,
	fee_currency TEXT,
	memo         TEXT,
	tx_hash      TEXT NOT NULL,
	sender       TEXT,
	recipient    TEXT NOT NULL,
        CONSTRAINT unique_deposits UNIQUE (tx_hash, recipient)
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE transactions;
