package db

import (
	"fmt"

	"bitbucket.org/canonical-ledgers/fct-address-monitord/flag"

	"github.com/gocraft/dbr"
	_ "github.com/mattn/go-sqlite3"
)

func newDBSession() (*dbr.Session, error) {
	conn, err := dbr.Open("sqlite3", flag.DBFile, nil)
	if err != nil {
		return nil, fmt.Errorf("dbr.Open(\"sqlite3\", \"%v\", nil): %v",
			flag.DBFile, err)
	}
	return conn.NewSession(nil), nil
}
