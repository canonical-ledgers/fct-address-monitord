package db

import (
	"errors"
	"fmt"
	"time"

	"github.com/canonical-ledgers/bitcointax"
	"github.com/gocraft/dbr"
)

var transactionsTbl = "transactions"

type transaction bitcointax.Transaction

func newFCTIncomeTransaction(date time.Time, volume float64,
	recipient, txHash string) bitcointax.Transaction {
	return bitcointax.Transaction{
		Date:      date,
		Action:    bitcointax.IncomeTx,
		Symbol:    "FCT",
		Currency:  "USD",
		Volume:    volume,
		Recipient: recipient,
		TxHash:    txHash,
		Memo:      "Authority Node Coinbase Payout",
	}
}

func (t transaction) String() string {
	return fmt.Sprintf("%v, Tx: %v Date: %v, "+
		"Price: %.3f %v, Amount: %.3f %v, Value: %.3f %v",
		t.Recipient[:8], t.TxHash[:6], t.Date.Format("2006-01-02 15:04:05 MST"),
		t.Price, t.Currency, t.Volume, t.Symbol, t.Total, t.Currency)
}

// creates an entry in the transactions table of the database.
func (t transaction) create(sess dbr.SessionRunner) error {
	_, err := sess.InsertInto(transactionsTbl).Columns(
		"date",
		"action",
		"symbol",
		"currency",
		"volume",
		"exchange",
		"exchange_id",
		"price",
		"total",
		"fee",
		"fee_currency",
		"memo",
		"tx_hash",
		"sender",
		"recipient",
	).Record(t).Exec()
	return err
}

func (t transaction) update(sess dbr.SessionRunner) error {
	if len(t.TxHash) == 0 {
		return errors.New("TxHash is empty")
	}
	res, err := sess.Update(transactionsTbl).
		Set("date", t.Date).
		Set("action", t.Action).
		Set("symbol", t.Symbol).
		Set("currency", t.Currency).
		Set("volume", t.Volume).
		Set("exchange", t.Exchange).
		Set("exchange_id", t.ExchangeID).
		Set("price", t.Price).
		Set("total", t.Total).
		Set("fee", t.Fee).
		Set("fee_currency", t.FeeCurrency).
		Set("memo", t.Memo).
		Set("tx_hash", t.TxHash).
		Set("sender", t.Sender).
		Set("recipient", t.Recipient).
		Where(dbr.Eq("tx_hash", t.TxHash)).Exec()
	if err != nil {
		return fmt.Errorf("sess.Update(%#v): %v", transactionsTbl, err)
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("res.RowsAffected(): %v", err)
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

func (t transaction) index() dbr.Builder {
	return dbr.And(
		dbr.Eq("tx_hash", t.TxHash),
		dbr.Eq("recipient", t.Recipient),
	)
}

func (t *transaction) read(sess dbr.SessionRunner) error {
	return sess.Select(
		"date",
		"action",
		"symbol",
		"currency",
		"volume",
		"exchange",
		"exchange_id",
		"price",
		"total",
		"fee",
		"fee_currency",
		"memo",
		"tx_hash",
		"sender",
		"recipient",
	).From(transactionsTbl).
		Where(t.index()).
		LoadStruct(t)
}
