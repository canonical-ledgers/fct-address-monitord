package db

import (
	"bitbucket.org/canonical-ledgers/fct-address-monitord/flag"

	"github.com/sirupsen/logrus"
)

var log *logrus.Entry

func setupLoggers() {
	_log := logrus.New()
	_log.Formatter = &logrus.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if flag.LogDebug {
		_log.SetLevel(logrus.DebugLevel)
	}
	log = _log.WithField("pkg", "db")
}
