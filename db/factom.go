package db

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/AdamSLevy/factom"
)

func getFCTTransactionsByHeight(height int64) ([]FCTTransaction, error) {
	block, err := factom.GetFBlockByHeight(height)
	if err != nil {
		return nil, fmt.Errorf("factom.GetHeights(%v): %v", height, err)
	}
	jsonBytes, err := json.Marshal(block.FBlock)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal() %v", err)
	}
	var fblock FBlock
	err = json.Unmarshal(jsonBytes, &fblock)
	if err != nil {
		return nil, fmt.Errorf("json.Unmarshal() %v", err)
	}

	return fblock.Transactions, nil
}

func factoshiToFactoid(i uint64) float64 {
	return float64(i) / 1e8
}

type FBlock struct {
	Transactions []FCTTransaction
}

type FCTTransaction struct {
	TxID    string `json:"txid"`
	Outputs []Output
	Date    timeT `json:"millitimestamp"`
}

type timeT time.Time

func (t *timeT) UnmarshalJSON(data []byte) error {
	var millitimestamp int64
	if err := json.Unmarshal(data, &millitimestamp); err != nil {
		return err
	}
	*t = timeT(time.Unix(millitimestamp/1e3, (millitimestamp%1000)*1e6))
	return nil
}

type Output struct {
	Amount  uint64
	Address string `json:"useraddress"`
}
