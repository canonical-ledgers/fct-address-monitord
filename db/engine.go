package db

import (
	"fmt"
	"time"

	"bitbucket.org/canonical-ledgers/fct-address-monitord/flag"
	"github.com/AdamSLevy/factom"
	"github.com/canonical-ledgers/bitcointax"
	"github.com/canonical-ledgers/cryptoprice"
	"github.com/gocraft/dbr"
)

var (
	sess         *dbr.Session
	errorExit    chan error
	cleanStop    chan error
	metadata     metadataT
	btctaxClient *bitcointax.Client
	cpriceClient *cryptoprice.Client
)

// Start() opens the database connection, sets up channels, and launches the
// listen() goroutine. Start returns the channel that will receive errors from
// the listen() goroutine.
func Start() (chan error, error) {
	setupLoggers()

	btctaxClient = bitcointax.NewClient(flag.BitcoinTaxKey, flag.BitcoinTaxSecret)
	btctaxClient.Timeout = flag.BitcoinTaxTimeout

	cpriceClient = cryptoprice.NewClient("FCT", "USD")
	cpriceClient.Timeout = flag.CryptoCompareTimeout
	cpriceClient.ExtraParams = "github.com/canonical-ledgers/fct-address-monitord"

	var err error
	sess, err = newDBSession()
	if err != nil {
		return nil, fmt.Errorf("newDBSession(): %v", err)
	}
	log.Debug("DB connection established.")

	// Load the application metadata
	if err := metadata.readOrCreate(sess); err != nil {
		return nil, fmt.Errorf("metadata.readOrCreate(): %v", err)
	}

	// Get current block height.
	heights, err := factom.GetHeights()
	if err != nil {
		return nil, fmt.Errorf("factom.GetHeights(): %v", err)
	}
	// If FBlockHeight has not been initialized then we just start from the
	// current block.
	if metadata.FBlockHeight == 0 {
		log.Debugf("metadata%+v.FBlockHeight is uninitialized.", metadata)
		metadata.FBlockHeight = heights.EntryHeight
		log.Debugf("Setting metadata.FBlockHeight = %v", heights.LeaderHeight-1)
	}

	if flag.StartScanHeight != 0 {
		if flag.StartScanHeight > heights.EntryHeight {
			return nil, fmt.Errorf("StartScanHeight (%v) is larger "+
				"than current EntryHeight (%v).",
				flag.StartScanHeight,
				heights.EntryHeight)
		}
		log.Debugf("Overriding metadata.FBlockHeight (%v) with "+
			"StartScanHeight (%v).",
			metadata.FBlockHeight,
			flag.StartScanHeight)
		metadata.FBlockHeight = flag.StartScanHeight
	}

	errorExit = make(chan error)
	cleanStop = make(chan error)

	// Scan latest blocks for new deposits.
	log.Infof("Scanning for deposits since last start up...")
	err = scanNewBlocksForDeposits()
	if err != nil {
		return nil, fmt.Errorf("scanNewBlocksForDeposits(): %v", err)
	}

	// Launch the channel listener thread.
	go listen()

	return errorExit, nil
}

// Stop() sends a stop signal to the listen() goroutine and then closes the
// database session.
func Stop() {
	cleanStop <- nil
	closeAll()
}

// errorStop() sends an error back to main() from the listen() goroutine.
func errorStop(err error) {
	log.Error(err)
	errorExit <- err
	closeAll()
}

func closeAll() {
	if err := sess.Close(); err != nil {
		log.Errorf("sess.Close(): %v", err)
	}
	close(errorExit)
	close(cleanStop)
}

var blockTime = 1 * time.Minute
var pageSize = uint64(200)

// This is the database engine runtime loop. This is the only goroutine that
// accesses the database to avoid any race conditions. It serializes tips and
// withdrawals sent over channels from the srv package. Deposits are detected
// by scanning each new block's transactions for outputs that match a user's
// deposit address. Any errors that are passed back to this function are sent
// back to main() over the errorExit channel. Main sends a nil error over the
// cleanStop channel when the program is shutting down.
func listen() {
	log.Debugf("Started db.listen() goroutine")
	blockTick := time.Tick(blockTime)
	for {
		select {
		case <-blockTick:
			err := scanNewBlocksForDeposits()
			if err != nil {
				errorStop(fmt.Errorf("scanNewBlocksForDeposits(): %v", err))
				return
			}
		case <-cleanStop:
			log.Debug("Clean stop signal received.")
			return
		}
	}
}

func scanNewBlocksForDeposits() error {
	// Get the current leader's block height
	heights, err := factom.GetHeights()
	if err != nil {
		return fmt.Errorf("factom.GetHeights(): %v", err)
	}
	currentHeight := heights.EntryHeight
	// Scan blocks from the last saved FBlockHeight up to but not including
	// the leader height
	for height := metadata.FBlockHeight; height < currentHeight; height++ {
		log.Debugf("Scanning block %v for deposits.", height)
		// Get the transactions from this block
		fctTransactions, err := getFCTTransactionsByHeight(height)
		if err != nil {
			return fmt.Errorf("getFCTTransactionsByHeight(%v): %v", height, err)
		}
		// Scan the block's FCT transactions for deposits
		if err := saveTransactions(
			scanFCTTransactionsForDeposits(fctTransactions)); err != nil {
			return fmt.Errorf("saveTransactions(txs) : %v", err)
		}
		metadata.FBlockHeight = height + 1
		if err := metadata.update(sess); err != nil {
			return fmt.Errorf("metadata%+v.update(): %v", metadata, err)
		}
	}

	return nil
}

func saveTransactions(txs []bitcointax.Transaction) error {
	if len(txs) == 0 {
		return nil
	}

	// Start DB transaction.
	var dbTx *dbr.Tx
	dbTx, err := sess.Begin()
	if err != nil {
		return fmt.Errorf("sess.Begin(): %v", err)
	}
	defer dbTx.RollbackUnlessCommitted()

	// Get prices and save to DB
	var totalIncome float64
	var totalFCT float64
	var txsToSend []bitcointax.Transaction
	for i, _ := range txs {
		tx := &txs[i]
		p, err := cpriceClient.GetPrice(time.Time(tx.Date))
		if err != nil {
			return fmt.Errorf("cryptoprice.Client.GetPrice(%v): %v",
				tx.Date, err)
		}
		tx.Price = p
		tx.Total = p * tx.Volume
		if err := (*transaction)(tx).read(dbTx); err == nil {
			log.Debugf("Already saved transaction: %v", transaction(*tx))
			continue
		}
		if err := transaction(*tx).create(dbTx); err != nil {
			return fmt.Errorf("transaction%+v.create(): %v", tx, err)
		}
		log.Info("New transaction ", transaction(*tx))
		txsToSend = append(txsToSend, *tx)
		totalIncome += tx.Total
		totalFCT += tx.Volume
	}

	// Update FBlockHeight
	metadata.TotalIncome += totalIncome
	metadata.TotalFCT += totalFCT
	if err := metadata.update(dbTx); err != nil {
		return fmt.Errorf("metadata%+v.update(): %v", metadata, err)
	}

	if len(txsToSend) == 0 {
		return nil
	}

	// Send to bitcoin.tax
	if err := btctaxClient.AddTransactions(txsToSend); err != nil {
		return fmt.Errorf("bitcointax.Client.AddTransactions(%+v): %v",
			txs, err)
	}

	return dbTx.Commit()
}

func scanFCTTransactionsForDeposits(fctTxs []FCTTransaction) []bitcointax.Transaction {
	txs := make([]bitcointax.Transaction, 0, 2)
	// For each factoid transaction...
	for _, ftx := range fctTxs {
		// For each output in a factoid transaction...
		for _, o := range ftx.Outputs {
			// See if any output address matches one of the target
			// addresses.
			match := false
			for _, a := range flag.Addresses {
				if o.Address == a {
					match = true
					break
				}
			}
			if !match {
				continue
			}
			txs = append(txs, newFCTIncomeTransaction(time.Time(ftx.Date),
				factoshiToFactoid(o.Amount), o.Address, ftx.TxID))
		}
	}
	return txs
}
