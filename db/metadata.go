package db

import (
	"database/sql"
	"fmt"

	"github.com/gocraft/dbr"
)

// User struct for database entries
type metadataT struct {
	ID           uint64
	FBlockHeight int64 `db:"fblock_height"`
	TotalIncome  float64
	TotalFCT     float64 `db:"total_fct"`
}

// Name of users table
var metadataTbl = "metadata"

func (m metadataT) index() dbr.Builder {
	return dbr.Eq("id", m.ID)
}

func (m *metadataT) create(sess dbr.SessionRunner) error {
	var res sql.Result
	res, err := sess.InsertInto(metadataTbl).Columns(
		"id",
		"fblock_height",
		"total_income",
	).Record(m).Exec()
	if err != nil {
		return err
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

func (m *metadataT) read(sess dbr.SessionRunner) error {
	return sess.Select("fblock_height", "total_income").
		From(metadataTbl).
		Where(m.index()).
		LoadStruct(m)
}

func (m *metadataT) readOrCreate(sess dbr.SessionRunner) error {
	if err := m.read(sess); err != nil {
		return m.create(sess)
	}
	return nil
}

func (m metadataT) update(sess dbr.SessionRunner) error {
	res, err := sess.Update(metadataTbl).
		Set("total_income", m.TotalIncome).
		Set("total_fct", m.TotalFCT).
		Set("fblock_height", m.FBlockHeight).
		Where(m.index()).Exec()
	if err != nil {
		return err
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}
